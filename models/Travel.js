const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const travelSchema = new Schema({

    name: { type: String, required: true },
    image: { type: String },
    company: { type: String, required: true },
    time: { type: String, required: true },
    stopover: { type: Number, required: true },
    price: { type: Number, required: true },

},{
    timestamps: true,
});


const Travel = mongoose.model('Travels', travelSchema);
module.exports = Travel;