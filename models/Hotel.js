const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const hotelSchema = new Schema({

    name: { type: String, required: true },
    location: { type: String, required: true },
    stars: { type: Number, required: true },
    image: { type: String,},
    price: { type: Number, required: true },

},{
    timestamps: true,
});


const Hotel = mongoose.model('Hotels', hotelSchema);
module.exports = Hotel;