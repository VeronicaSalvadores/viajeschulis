const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({

    email: { type: String, required: true },
    password: { type: String, required: true },
    role: {
        enum: ["admin", "user"],
        type: String,
        default: "user",
        required: true,
    },
    travels: [{
        type: mongoose.Types.ObjectId,
        ref: 'Travels',
    }]
    },{
        
    timestamps: true,
});

const User = mongoose.model('Users', userSchema);
module.exports = User;