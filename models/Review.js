const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const reviewsSchema = new Schema({

    name: { 
        type: String,
        required: true,
    },

    title: { 
        type: String,
        required: true,
    },

    description: { 
        type: String,
        required: true,
    },
    },{
        
    timestamps: true,

});

const Review = mongoose.model('Reviews', reviewsSchema);
module.exports = Review;