const Travel = require('../models/Travel');
const Hotel = require('../models/Hotel');

const controller = {};

//VIAJES
controller.travelsFormGet = (req, res, next) => {
    res.status(200).render('create-travels');
},

controller.travelsFormPost = async (req, res, next) => {
    try{
        const {
            name,
            image,
            company,
            time,
            stopover,
            price,            
        } = req.body;

        const newTravel = new Travel({
            name,
            image: req.file_url,
            company,
            time,
            stopover: Number(stopover),
            price: Number(price),
        });

        const createdTravel = await newTravel.save();

        return res.status(201).redirect(`/travels/${createdTravel._id}`);
    } catch (error) {
        next(error);
    }
}

//HOTELES
controller.hotelsFormGet = (req, res, next) => {
    res.status(200).render('create-hotels');
},

controller.hotelsFormPost = async (req, res, next) => {
    try{
        const {
            name,
            location,
            stars,
            image,
            price,            
        } = req.body;

        const newHotel = new Hotel({
            name,
            image: req.file? `/uploads/${req.file.filename}` : null,
            location,
            stars: Number(stars),
            price: Number(price),
        });

        const createdHotel = await newHotel.save();

        return res.status(201).redirect(`/hotels/${createdHotel._id}`);
    } catch (error) {
        next(error);
    }
}

module.exports = controller;