const controller = {};

controller.indexGet = (req, res, next) => {
    return res.status(200).render('index', {title: 'Agencia de viajes', isAdmin: req.user.role === 'admin'})
}

controller.registerGet = (req, res, next) => {
    return res.render('register')
}

controller.loginGet = (req, res, next) => {
    return res.render('login')
}

module.exports = controller;