const Review = require('../models/Review');

const controller = {}

controller.reviewsGet = async (req, res, next) => {
    
    try{
        const reviews = await Review.find();
        return res.status(200).render('reviews', {reviews});

    }catch(error){
        next(error);
    }

},

controller.reviewsPost = async (req, res, next) => {
    
    try{
        const {
            name,
            title,
            description,       
        } = req.body;

        const newReview = new Review({
            name,
            title,
            description,
        });

        const createdReview = await newReview.save();
        return res.status(201).redirect('/reviews');
    } catch (error) {
        next(error);
    }
}

module.exports = controller;