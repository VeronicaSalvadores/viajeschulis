const Hotel = require('../models/Hotel');

const controller = {};

controller.hotelsGet = async (req, res) => {
    try{
        const hotels = await Hotel.find();
        return res.status(200).render('hotels', {hotels})
    }catch(error) {
        return res.status(500).json(error);
    }
},

controller.hotelsIdGet = async (req, res, next) => {
    const id = req.params.id;

    try {
        let hotelFinded = await Hotel.findById(id);

        if(!hotelFinded){
            hotelFinded = false;
        }
        return res.status(200).render('hotel', { title: `${hotelFinded.name || 'No travel found'} page`, hotels: hotelFinded, id});
    }catch(error){
        next(error);
    }
},

controller.editHotelPut = async (req, res, next) => {
    try {
        const { 
            
            name,
            location, 
            stars, 
            image, 
            price, 
            id } = req.body;

        const updateHotel = await Hotel.findByIdAndUpdate(
            id,
            { name, 
            location, 
            stars, 
            image, 
            price  },
            { new: true }
            );

            return res.redirect(`/hotels/edit/${updateHotel._id}`);
    } catch(error) {
        next(error);
    }
},

controller.hotelIdGet = async (req, res, next) => {
    const id = req.params.id;

    try {
        let hotelFinded = await Hotel.findById(id);

        if(!hotelFinded){
            hotelFinded = false;
        }
        return res.status(200).render('hotel-edit', { title: `${hotelFinded.name || 'No travel found'} page`, hotels: hotelFinded, id});
    }catch(error){
        next(error);
    }
},

controller.deleteHotels = async (req, res, next) => {
    const { id } = req.params;

    try{
        await Hotel.findByIdAndDelete(id);

        res.redirect("/hotels");

    }catch(error){
        next(error);
    }
}

module.exports = controller;