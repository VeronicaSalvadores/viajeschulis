const Travel = require('../models/Travel');

const controller = {};

controller.travelsGet = async (req, res) => {
    try{
        const travels = await Travel.find();
        return res.status(200).render('travels', {travels})
    }catch(error) {
        return res.status(500).json(error);
    }
},

controller.travelsIdGet = async (req, res, next) => {
    const id = req.params.id;

    try {
        let travelFinded = await Travel.findById(id);

        if(!travelFinded){
            travelFinded = false;
        }
        return res.status(200).render('travel', { title: `${travelFinded.name || 'No travel found'} page`, isAdmin: req.user.role === 'admin', travels: travelFinded, id});
    }catch(error){
        next(error);
    }
},

controller.editTravelPost = async (req, res, next) => {
    
    try {
        const id = req.params.id;

        const { 
            name,
            company, 
            time, 
            stopover, 
            price, 
            } = req.body;

        const updateTravel = await Travel.findByIdAndUpdate(
            id,
            { name, 
            company, 
            time, 
            stopover, 
            price  },
            { new: true }
            );

            return res.redirect(`/travels/${this._id}`);
    } catch(error) {
        next(error);
    }
},

controller.travelEditIdGet = async (req, res, next) => {
    const id = req.params.id;

    try {
        let travelFinded = await Travel.findById(id);

        if(!travelFinded){
            travelFinded = false;
        }
        return res.status(200).render('travel-edit', { title: `${travelFinded.name || 'No travel found'} page`, travels: travelFinded, id});
    }catch(error){
        next(error);
    }
},

controller.deleteTravels = async (req, res, next) => {
    const { id } = req.params;

    try{
        await Travel.findByIdAndDelete(id);

        res.redirect("/travels");

    }catch(error){
        next(error);
    }
}

module.exports = controller;