const passport = require("passport");

const controller = {};

controller.registerPost = (req, res, next) => {
    passport.authenticate('register', (error, user) => {
        if(error) {
            return res.render('register', {error: error.message});
        }
        req.login(user, (error) => {
            if(error){
                return res.render('register', {error: error.message});
            }
            return res.redirect('/login')
        })
    })(req, res, next);
},

controller.loginPost = (req, res, next) => {
    passport.authenticate('login', (error, user) => {
        if(error) {
            return res.render('login', {error: error.message});
        }

        req.login(user, (error) => {
            if(error) {
                return res.render('login', {error: error.message})
            }
            return res.redirect('/');
        })
    })(req, res, next);
},

controller.logoutPost = (req, res, next) => {
    if (req.user) {
      req.logout();

      req.session.destroy(() => {
        res.clearCookie("connect.sid");

        return res.redirect('/');
      });
    } else {
      return res.sendStatus(304);
    }
};

module.exports = controller;