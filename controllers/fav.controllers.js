const User = require('../models/User');

const controller = {}

controller.addFavGet = async (req, res, next) => {   
    
    try{
        const users = await User.findById(req.user._id).populate("travels");
        const {travels} = users;
        return res.status(200).render('favs', {travels});

    }catch(error) {
        next(error);
    }
}

controller.addFavPost = async (req, res, next) => {
    
    try {
        const { travelId } = req.body;
        const userId = req.user._id;

        const addFavs = await User.findByIdAndUpdate(
            userId,
            { $addToSet: { travels: travelId } },  
            { new: true }
        );

        return res.status(200).redirect('/favs');                                                        
            
    } catch (error) {
        next(error);
    }
}

module.exports = controller;