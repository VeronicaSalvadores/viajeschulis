const mongoose = require('mongoose');
require('dotenv').config();

const DB_URL = `mongodb+srv://${process.env.MONGO_DB_USER}:${process.env.MONGO_DB_PASSWORD}@viajes-cluster.2gzwa.mongodb.net/agenciaviajes?retryWrites=true&w=majority`;

const connect = () => {

    mongoose.connect(DB_URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    })
    .then(() => {
    console.log('Connected to DB');
    })
    .catch((error) => {
    console.log('Error connecting to DB', error);
    })
};

module.exports = { DB_URL, connect };