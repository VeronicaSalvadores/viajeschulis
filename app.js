const express = require('express');
const path = require('path');
const mongoose = require('mongoose');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const passport = require('passport');
const userRouter = require('./routes/user.routes');
const indexRouter = require('./routes/index.routes');
const travelRouter = require('./routes/travel.routes');
const hotelRouter = require('./routes/hotel.routes');
const createRouter = require('./routes/create.routes');
const favRouter = require('./routes/fav.routes');
const reviewsRouter = require('./routes/reviews.routes');

require('dotenv').config();
require('./helpers/hbs.helpers');
require('./db.js');
require('./passport');

const PORT = process.env.PORT || 4000;
const app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(express.static(path.join(__dirname, 'public')));

app.use(
  session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
    cookie: {
      maxAge: 3600000000,
    },
    store: new MongoStore({ mongooseConnection: mongoose.connection }),
  })
);

app.use(passport.initialize());
app.use(passport.session());

app.use("/", indexRouter);
app.use("/users", userRouter);
app.use("/travels", travelRouter);
app.use("/hotels", hotelRouter);
app.use("/create", createRouter);
app.use("/favs", favRouter);
app.use("/reviews", reviewsRouter);

const router = express.Router();

app.use('*', (req, resp, next) => {
  const error = new Error('Route not found');
  error.status = 404;
  next(error);
})

app.use ((err, req, res, next) => {
  return res.status(err.status || 500).json(err.message || 'Unexpected error');
})

app.listen(PORT, () => {
  console.log(`Listening in http://localhost:${PORT}`);
});require('dotenv').config();