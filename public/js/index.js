const deleteTravel = (id) => {
    fetch(`/travels/${id}`,{
        method: 'DELETE'
    });
}

const deleteHotel = (id) => {
    fetch(`/hotels/${id}`,{
        method: 'DELETE'
    });
}

