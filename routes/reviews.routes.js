const express = require('express');
const authMiddleware = require('../middlewares/auth.middleware');
const reviewsController = require('../controllers/reviews.controllers');

const router = express.Router();

//Crear reseña
router.get('/', [authMiddleware.isAuthenticated], reviewsController.reviewsGet);
router.post('/', [authMiddleware.isAuthenticated], reviewsController.reviewsPost);

module.exports = router;