const express = require('express');
const authMiddleware = require('../middlewares/auth.middleware');
const hotelsController = require('../controllers/hotel.controllers');

const router = express.Router();

//Vista general y detalles
router.get('/', [authMiddleware.isAuthenticated] ,hotelsController.hotelsGet);
router.get('/:id', [authMiddleware.isAuthenticated] ,hotelsController.hotelsIdGet);

//Ruta modificar
router.put('/edit/:id', [authMiddleware.isAuthenticated, authMiddleware.isAdmin], hotelsController.editHotelPut);
router.get('/edit/:id', [authMiddleware.isAuthenticated, authMiddleware.isAdmin], hotelsController.hotelIdGet);


//Ruta eliminar
router.get('/delete/:id', [authMiddleware.isAuthenticated, authMiddleware.isAdmin], hotelsController.deleteHotels);

module.exports = router;