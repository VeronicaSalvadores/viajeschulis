const express = require('express');
const authMiddleware = require('../middlewares/auth.middleware');
const travelsController = require('../controllers/travels.controllers');

const router = express.Router();

//Vista general y detalles
router.get('/', [authMiddleware.isAuthenticated], travelsController.travelsGet);
router.get('/:id', [authMiddleware.isAuthenticated], travelsController.travelsIdGet);

//Ruta modificar
router.post('/edit/:id', [authMiddleware.isAuthenticated, authMiddleware.isAdmin] ,travelsController.editTravelPost);
router.get('/edit/:id', [authMiddleware.isAuthenticated, authMiddleware.isAdmin] ,travelsController.travelEditIdGet);


//Ruta eliminar
router.get('/delete/:id', [authMiddleware.isAuthenticated, authMiddleware.isAdmin] ,travelsController.deleteTravels);

module.exports = router;