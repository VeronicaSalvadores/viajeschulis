const express = require('express');
const authMiddleware = require('../middlewares/auth.middleware');
const fileMiddleware = require('../middlewares/file.middleware');
const createController = require('../controllers/create.controllers');

const router = express.Router();

//Crear viajes
router.get('/travels', [authMiddleware.isAuthenticated, authMiddleware.isAdmin], createController.travelsFormGet);
router.post('/travels', [authMiddleware.isAuthenticated, authMiddleware.isAdmin, fileMiddleware.upload.single('image'), fileMiddleware.uploadToCloudinary] ,createController.travelsFormPost);

//Crear hoteles
router.get('/hotels', [authMiddleware.isAuthenticated, authMiddleware.isAdmin], createController.hotelsFormGet);
router.post('/hotels', [authMiddleware.isAuthenticated, authMiddleware.isAdmin, fileMiddleware.upload.single('image'), fileMiddleware.uploadToCloudinary] ,createController.hotelsFormPost);

module.exports = router;