const express = require('express');
const authMiddleware = require('../middlewares/auth.middleware');
const favController = require('../controllers/fav.controllers');

const router = express.Router();

//Wishlist
router.get('/', [authMiddleware.isAuthenticated], favController.addFavGet);
router.post('/', [authMiddleware.isAuthenticated], favController.addFavPost);

module.exports = router;