const express = require('express');
const authMiddleware = require('../middlewares/auth.middleware');
const indexController = require('../controllers/index.controllers');
const router = express.Router();

router.get('/', [authMiddleware.isAuthenticated], indexController.indexGet);
router.get('/register', indexController.registerGet);
router.get('/login', indexController.loginGet);

module.exports = router;