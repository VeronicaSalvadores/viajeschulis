const mongoose = require('mongoose');
const DB_URL = require('../db.js');
const Hotel = require('../models/Hotel');

const hotels = [
    //HOTELES AMERICA
  {
    name: "Holiday Inn New York City - Times Square",
    location: "Nueva York",
    stars: 3,
    image: "/images/hotels/nuevayork.jpeg",
    price: 53,
  },
  {
    name: "Hotel The Ritz Carlton Los Ángeles",
    location: "Los Ángeles",
    stars: 5,
    image: "/images/hotels/losangeles.jpeg",
    price: 350,
  },
  {
    name: "Meliá Habana Hotel",
    location: "La Habana",
    stars: 5,
    image: "/images/hotels/lahabana.jpeg",
    price: 96,
  },
  //HOTELES EUROPA
  {
    name: "Hotel Hôtel La Nouvelle République",
    location: "París",
    stars: 3,
    image: "/images/hotels/paris.jpeg",
    price: 100,
  },
  {
    name: "Riu Plaza Berlin",
    location: "Berlín",
    stars: 4,
    image: "/images/hotels/berlin.jpeg",
    price: 65,
  },
  {
    name: "Wynn's Hotel Dublin",
    location: "Dublín",
    stars: 3,
    image: "/images/hotels/dublin.jpeg",
    price: 119,
  },
  //HOTELES ASIA
  {
    name: "Grand Inna Bali Beach",
    location: "Bali",
    stars: 5,
    image: "/images/hotels/bali.jpeg",
    price: 18,
  },
  {
    name: "Shinjuku Granbell Hotel",
    location: "Tokio",
    stars: 4,
    image: "/images/hotels/tokio.jpeg",
    price: 44,
  },
  {
    name: "Hathaa Beach Maldives",
    location: "Malé",
    stars: 3,
    image: "/images/hotels/maldivas.jpeg",
    price: 64,
  },
  //HOTELES AFRICA
  {
    name: "Hotel la mada",
    location: "Kenia",
    stars: 4,
    image: "/images/hotels/kenia.jpeg",
    price: 94,
  },
  {
    name: "Steigenberger Hotel El Tahrir Cairo",
    location: "El Cairo",
    stars: 4,
    image: "/images/hotels/elcairo.jpeg",
    price: 36,
  },
  {
    name: "Hilton Cabo Verde Sal Resort",
    location: "Cabo Verde",
    stars: 5,
    image: "/images/hotels/caboverde.jpeg",
    price: 154,
  },
];

mongoose.connect(DB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(async () => {
 
    const allHotels = await Hotel.find();

    if (allHotels.length) {
      await Hotel.collection.drop();
    }
  })
  .catch((err) => {
    console.log(`Error deleting db data ${err}`);
  })
  .then(async () => {

    await Hotel.insertMany(hotels);
    console.log("Seed saved in DB");
  })
  .catch((err) => {

    console.log(`Error adding data to our db ${err}`);
  })

  .finally(() => mongoose.disconnect());