const mongoose = require('mongoose');
const DB_URL = require('../db.js');
const Travel = require('../models/Travel');

const travels = [
    //VIAJES AMERICA
  {
    name: "Los Ángeles (EEUU)",
    image: "/images/travels/losangeles.png",
    company: "Iberia",
    time: "14 horas",
    stopover: 0,
    price: 500,
  },
  {
    name: "Nueva York (EEUU)",
    image: "/images/travels/newyork.jpeg",
    company: "Air Europe",
    time: "7 horas",
    stopover: 0,
    price: 400,
  },
  {
    name: "Miami (EEUU)",
    image: "/images/travels/miami.jpeg",
    company: "American Airlines",
    time: "10 horas",
    stopover: 0,
    price: 300,
  },
  {
    name: "Cancún (México)",
    image: "/images/travels/cancun.jpeg",
    company: "Star Alliance",
    time: "15 horas",
    stopover: 1,
    price: 350,
  },
  {
    name: "La Habana (Cuba)",
    image: "/images/travels/lahabana.jpeg",
    company: "British Airways",
    time: "15 horas",
    stopover: 1,
    price: 350,
  },
  //VIAJES EUROPA
  {
    name: "París (Francia)",
    image: "/images/travels/paris.jpeg",
    company: "Iberia",
    time: "2 horas",
    stopover: 0,
    price: 60,
  },
  {
    name: "Berlín (Alemania)",
    image: "/images/travels/berlin.jpeg",
    company: "Air Europe",
    time: "3 horas",
    stopover: 0,
    price: 70,
  },
  {
    name: "Milán (Italia)",
    image: "/images/travels/milan.jpeg",
    company: "American Airlines",
    time: "1 hora",
    stopover: 0,
    price: 70,
  },
  {
    name: "Dublín (Irlanda)",
    image: "/images/travels/dublin.jpeg",
    company: "Star Alliance",
    time: "2 horas",
    stopover: 0,
    price: 100,
  },
  {
    name: "Ámsterdam (Países Bajos)",
    image: "/images/travels/amsterdam.jpeg",
    company: "British Airways",
    time: "3 horas",
    stopover: 0,
    price: 500,
  },
  //VIAJES ASIA
  {
    name: "Denpasar (Bali)",    
    image: "/images/travels/denpasar.jpeg",
    company: "Iberia",
    time: "20 horas",
    stopover: 2,
    price: 750,
  },
  {
    name: "Tokio (Japón)",
    image: "/images/travels/tokio.jpeg",
    company: "Air Europe",
    time: "17 horas",
    stopover: 1,
    price: 400,
  },
  {
    name: "Malé (Maldivas)",
    image: "/images/travels/male.jpeg",
    company: "American Airlines",
    time: "14 horas",
    stopover: 1,
    price: 700,
  },
  {
    name: "Dubái (Emiratos Árabes)",
    image: "/images/travels/dubai.jpeg",
    company: "Star Alliance",
    time: "14 horas",
    stopover: 1,
    price: 400,
  },
  {
    name: "Ammán (Jordania)",
    image: "/images/travels/amman.jpeg",
    company: "British Airways",
    time: "10 horas",
    stopover: 1,
    price: 500,
  },
  //VIAJES ÁFRICA
  {
    name: "El Cairo (Egipto)",
    image: "/images/travels/elcairo.jpeg",
    company: "Iberia",
    time: "8 horas",
    stopover: 1,
    price: 300,
  },
  {
    name: "Nairobi (Kenia)",
    image: "/images/travels/nairobi.jpeg",
    company: "Air Europe",
    time: "14 horas",
    stopover: 1,
    price: 500,
  },
  {
    name: "Kilimanjaro (Tanzania)",
    image: "/images/travels/kilimanjaro.jpeg",
    company: "American Airlines",
    time: "14 horas",
    stopover: 1,
    price: 400,
  },
  {
    name: "Isla de Sal (Cabo Verde)",
    image: "/images/travels/caboverde.jpg",
    company: "Star Alliance",
    time: "14 horas",
    stopover: 1,
    price: 400,
  },
  {
    name: "Carthage (Túnez)",
    image: "/images/travels/tunez.jpeg",
    company: "British Airways",
    time: "2 horas",
    stopover: 0,
    price: 150,
  },
];

mongoose.connect(DB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(async () => {
 
    const allTravels = await Travel.find();

    if (allTravels.length) {
      await Travel.collection.drop();
    }
  })
  .catch((err) => {
    console.log(`Error deleting db data ${err}`);
  })
  .then(async () => {

    await Travel.insertMany(travels);
    console.log("Seed saved in DB");
  })
  .catch((err) => {

    console.log(`Error adding data to our db ${err}`);
  })

  .finally(() => mongoose.disconnect());